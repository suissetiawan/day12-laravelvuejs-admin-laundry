@extends('layouts.master')
@section('content')
<div id="app" class="row">
<div class="col-md-4 grid-margin">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Input data Laundry</h4>
      <div class="forms-sample">
        <div class="form-group row">
          <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Name</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" placeholder="customer name" v-model="newData.customer">
          </div>
        </div>
        <div class="form-group row">
          <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Paket</label>
          <div class="col-sm-9">
            <select class="form-control" v-model="newData.paket">
	          <option selected>cuci basah</option>
	          <option>cuci kering</option>
	          <option>cuci setrika</option>
	        </select>
          </div>
        </div>
        <div class="form-group row">
          <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Berat</label>
          <div class="col-sm-9">
  	        <div class="input-group mb-2 mr-sm-2">
              <div class="input-group-prepend">
                <div class="input-group-text">Kg</div>
              </div>
              <input type="text" class="form-control" placeholder="total payment" v-model="newData.berat">

            </div>
          </div>
        </div>
        <div class="form-group row">
          <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Total</label>
          <div class="col-sm-9">
  	        <div class="input-group mb-2 mr-sm-2">
              <div class="input-group-prepend">
                <div class="input-group-text">Rp.</div>
              </div>
              <input type="text" class="form-control" placeholder="total payment" v-model="newData.total">
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label for="exampleInputUsername2" class="col-sm-3 col-form-label">payment status</label>
          <div class="col-sm-9">
            <select class="form-control" v-model="newData.pembayaran">
	          <option>belum lunas</option>
	          <option>lunas</option>
	        </select>
          </div>
        </div>
        <button class="btn btn-primary mr-2" @click="addData">Submit</button>
      </div>
    </div>
  </div>
</div>
<div class="col-lg-8 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Data Laundry</h4>
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>No</th>
              <th>Status Order</th>
              <th>Customers</th>
              <th>Paket</th>
              <th>Pembayaran</th>
              <th>Total</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="(data,index) in laundry">
              <td>@{{index+1}}</td>
              <td>
				<button type="button" class="btn btn-social-icon-text btn-sm btn-secondary pl-1" @click="toggleStatus(data)" :class="{'btn-success': data.done}">
					<i class="mdi mdi-sm mdi-check-circle-outline"></i>@{{data.status}}
				</button>
              </td>
              <td>@{{data.customer}}</td>
              <td>@{{data.paket}}</td>
              <td>@{{data.pembayaran}}</td>
              <td>Rp.@{{data.total}}(@{{data.berat}}kg)</td>
              <td>
              	<button class="btn btn-inverse-danger btn-sm" data-toggle="modal" :data-target="['#modal'+index]">hapus</button>
              	<div :id="['modal'+index]" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				 <div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalCenterTitle"><b>Perhatian !!</b></h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						</div>
						<div class="modal-body">
							<p class="mb-0">Apakah anda yakin menghapus @{{data.customer}}??</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn  btn-secondary" data-dismiss="modal">Close</button>
							<button type="button" class="btn  btn-danger" data-dismiss="modal" @click="removeData(index,data.id)">hapus</button>
						</div>
					</div>
				</div>
			</div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
@endsection

@push('script')
<script>
	let data = new Vue({
		el:'#app',
		data:{
			laundry:[],
		newData:{status: 'belum',customer:'',paket:'cuci basah',pembayaran:'belum lunas',berat:0,total:0},
		},
		methods:{
			addData: function(){
				let dataInput = this.newData;
				if (dataInput.customer) {
					this.$http.post('/api/data-laundry',dataInput).then(response=>{
						this.laundry.unshift(dataInput)
						this.newData = {status: 'belum',customer:'',paket:'cuci basah',pembayaran:'belum lunas',berat:0,total:0}
						Swal.fire({
				        icon: 'success',
				        title: 'Customer berhasil ditambah',
				        showConfirmButton: false,
					    timer: 1500
				      })
					});
				}
			},
			removeData: function(index,id){
				this.laundry.splice(index,1)
					Swal.fire({
					        icon: 'success',
					        title: 'Berhasil dihapus',
					        showConfirmButton: false,
						    timer: 1500
					      })
				
				this.$http.post('/api/hapus-laundry/'+id).then(response=>{
					
				})
			},
			toggleStatus: function(status){
				status.done = !status.done
				if (status.done) {
					status.status = 'sudah'
				} else {
					status.status = 'belum'
				}
				this.$http.post('/api/done-change/'+status.id).then(response=>{
				})	
			}
		},
		mounted : function(){
			this.$http.get('/api/data-laundry').then(response =>{
				//console.log(response.body.data)
				let data = response.body.data;
				this.laundry = data;
			});
		}
	})
</script>
@endpush