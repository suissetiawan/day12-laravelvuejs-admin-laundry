<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatalaundriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datalaundries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('customer');
            $table->string('status')->default('belum');
            $table->string('paket')->nullabel();
            $table->string('pembayaraan');
            $table->integer('berat_barang')->default(0);
            $table->integer('harga')->default(0);
            $table->tinyInteger('done');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datalaundries');
    }
}
