<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DatalaundryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=> $this->id,
            'customer' => $this->customer,
            'status' => $this->status,
            'tgl' => $this->created_at->format('d-m-Y'),
            'paket' => $this->paket,
            'berat' => $this->berat_barang,
            'pembayaran' => $this->pembayaraan,
            'total' => $this->harga,
            'done' => $this->done,
        ];
    }
}
