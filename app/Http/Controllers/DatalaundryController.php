<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Datalaundry;
use App\Http\Resources\DatalaundryResource;


class DatalaundryController extends Controller
{
    //
    public function index(){
    	$data = Datalaundry::orderBy('created_at', 'desc')->get();
    	return DatalaundryResource::collection($data);
    }

    public function store(Request $request){
    //dd($request->all());
    $data = Datalaundry::create([
            'customer' => $request->customer,
            'status' => $request->status,
            'paket' => $request->paket,
            'berat_barang' => $request->berat,
            'pembayaraan' => $request->pembayaran,
            'harga' => $request->total,
            'done' => 0,
    	]);

    return new DatalaundryResource($data);
    }

       public function destroy($id){
    	Datalaundry::destroy($id);

    	return response('Delete success');
    }


    public function changeDone($id){
    	$data = Datalaundry::find($id);

    	if ($data->done == 1) {
    		$update = 0;
    		$status = 'belum';
    	} else {
    		$update = 1;
    		$status = 'sudah';
    	}

    	$data->update([
    		'done' => $update,
    		'status' => $status
    	]);
    	
    	return new DatalaundryResource($data);
    }


}
